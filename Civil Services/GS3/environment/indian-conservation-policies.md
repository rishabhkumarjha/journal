[[_TOC_]]

# Indian Conservation Policies
```plantuml
@startmindmap
* Policies
** WPA-1972
*** Wildlife Sanctuary
*** National Parks
*** Shifted Wildlife subj \nfrm State->Concurrent
*** Schedules I-VI
*** Authoriites
****_ State
***** Chief Wildlife Warden
****_ Central
***** Central Zoo Authority
***** Wildlife Crime Control Bureau
***** National Tiger Consumer Authority
** EPA-1986
*** To Implement Stockholm Conf 1972
*** Brought under article 253\nLaws implementing International laws
**** Hence highly centralised
*** Powers Granted
**** Regulation of Pollution Discharge
**** Lay down env standards
*** Acts and Notfications
**** National Env Appellate Authority & NGT
**** Costal Regulation Zone Notifications
**** Env Impact Assesment
** BDA-2002
*** Implements UN Convention on Bio Diversity
*** Conservation & Sustainable use of Bio Diversity
*** Fair and Equitable Share of benifits biological Resources
*** Heavy compliance burden on medicine innovators\nall offences congnizavle & non-bailable
*** Authorities\nRegulate Supervise and Research
**** National Biodiversity Authority
**** State Biodiversity Authority
**** Biodiversity Management Committees
*** Biodiversity Heritage Sites\nNotified by State
@endmindmap
```