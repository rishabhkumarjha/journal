[[_TOC_]]
# Climate Change Types
```plantuml
@startmindmap
* Climate Change
** Global Warming
*** Green House Gas
**** CO<sub>2</sub>
**** Black Carbon <&clock> months
***** on snow absorbs heat\nburning soot fuels
**** CH<sub>4</sub> <&fire>21 <&clock> 12
***** Agri biggest contrib\nthrough manure & livestock
**** N<sub>2</sub>O <&fire>310 <&clock> 120
***** Fertiliser prod and use\ndecomposition by bacteria
**** SO<sub>2</sub>
**** CFC/HFC <&fire>140-11,700 <&clock> <270
**** Per Fluro Carbon <&fire>>6,500 <&clock> >800
***** Al/semicondoctor prod.
**** SF<sub>6</sub> <&fire>23,900 <&clock> 3200
*** Climate Forcings\n+ve and -ve
** Ocean<sub>(pH=8)</sub> Acidification\n=Ocean sinks 1/3 CO<sub>2</sub>
*** acidification inc H+ forming HCO<sub>3</sub> reducing CO<sub>3</sub>
**** inhibits calcification by organisms
**** lower absorption of CO<sub>2</sub>
**** **Saturation<sub>CO3</sub> Horizon**\nbecomes shallower
***** disolving shells of \ndeeper org
***** upwelling exposes \nupper org as well
** Ozone Depletion
*** Prominent in Antarctica\n**Colder** than arctic enables stratospheric cloud formations\nPolar Vortex confines it
*** Agents
**** CFC/HBFCs\n<&clock>40-50yrs
**** Oxidised by O<sub>3</sub>
**** NO
***** N<sub>2</sub>O photolysis -> NO->catalyses O<sub>3</sub> destruction
@endmindmap
```
# Climate Change Mitigation
```plantuml
@startmindmap
* Mitigation Strategy
** Carbon Sequestration
*** Green Carbon\nAbsorbed by Plants
*** <&droplet> Blue Carbon\nAbsorbed in Aquatic,Marine & Costal
** Carbon Credit
*** Cap and Trade
*** In India [[{Multi Commodities Index} MCX]] Trades
** Carbon Offset
*** Meeting Carbon reduction by investing CO<sub>2</sub> reduction project
** Carbon Tax
*** <&check>Taxing emmission **Most Simple**
*** <&x>Taxing by developed countries on developing country's goods\nhurts industrialisation efforts
** Geo Engineering
*** Spewing atmosphere with SO<sub>2</sub> imitating volcano
*** Space Mirrors
*** Iron Seeding of Seas\nAccelarate Plankton population
*** Cloud Whitening
@endmindmap
```