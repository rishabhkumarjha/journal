[[_TOC_]]
# Human Geography
Inter relationship between human and geography and its impact.
Naturalisation of Human vs Humanisation of Nature
## Population Patterns
90% world population lives on 10% landmass
### Population Matrix
```math
\begin{align}
\text{Natural Growth} &= Births-Deaths\\
\text{Actual Growth} &= Births - Deaths + immigration - emmigration \\
\text{Crude Birth Rate(CBR)} & = {\text{Live Birth}\over{Population}} \times 1000 \\
\text{CDR} &= {\text{Deaths}\over{Population}} \times 1000
\end{align}
```
### Demographic Transition
![](https://i2.wp.com/devlibrary.in/wp-content/uploads/2021/04/image-44.jpeg?resize=474%2C388&ssl=1)
### Thomas Malthus Theory and Economics as Dismal Science(Thomas Carlyle)
The exponential population growth will surpass arithmatic food growth which would ultimately require reduction in human population(by wars, dieseases and famine) to sustainable levels.
However the technological growth surpassed these predictions and fueled population groth thus far.